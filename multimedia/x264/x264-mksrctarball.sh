#!/bin/bash

git clone https://code.videolan.org/videolan/x264.git

cd x264
  git checkout stable
#   VERSION="git_$(git log --format="%ad_%h" --date=short | head -n 1 | tr -d -)"
  VERSION="git_$(git describe --tags --always | tr '-' '_')"
  LONGDATE="$(git log -1 --format=%cd --date=format:%c )"
  git archive --format=tar --prefix=x264-$VERSION/ HEAD >../x264-$VERSION.tar
cd ..

plzip -9 -v x264-$VERSION.tar
touch -d "$LONGDATE" x264-$VERSION.tar.lz
rm -rf x264-$VERSION
md5sum x264-$VERSION.tar.lz
