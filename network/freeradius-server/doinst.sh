config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

preserve_perms() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  if [ -e $OLD ]; then
    cp -a $OLD ${NEW}.incoming
    cat $NEW > ${NEW}.incoming
    mv ${NEW}.incoming $NEW
  fi
  config $NEW
}

if ! getent group | grep -q "^radius:"; then
  echo "Add freeradius daemon group"
  groupadd -g 272 radius
fi
if ! getent passwd | grep -q "^radius:"; then
  echo "Add freeradius daemon user"
  useradd -g radius -u 272 -d /dev/null -c 'FreeRADIUS Daemon User' -s /bin/false radius
fi

preserve_perms etc/rc.d/rc.radiusd.new
config etc/raddb/clients.conf.new;
config etc/raddb/dictionary.new;
config etc/raddb/experimental.conf.new;
config etc/raddb/panic.gdb.new;
config etc/raddb/radiusd.conf.new;
config etc/raddb/templates.conf.new;
config etc/raddb/trigger.conf.new;
