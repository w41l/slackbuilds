nginx [engine x] is a high-performance HTTP server and reverse proxy
as well as an IMAP/POP3 proxy server.

By default, nginx will use the "nobody" user and group accounts. You may
specify alternate values on the command line if desired; for example:

  NGINXUSER=apache NGINXGROUP=apache ./nginx.SlackBuild

Regardless of which user and group you decide to use, you will need to
make sure they exist on both the build system and the target system.

Geoip support is now available as an option using the GeopIP
package. By default GEOIP module will built as dynamic shared object (DSO).
If you wish to enable GeoIP the pass GEOIP variable to the slackbuild:

  GEOIP=yes ./nginx.SlackBuild

Support for gperftools is available as an option using the gperftools
package. If you wish to enable gperftools pass the GPERF variable to
the slackbuild:

  GPERF=yes ./nginx.SlackBuild

Enable fancy indexing by passing FANCYINDEX variable to the slackbuild:

  FANCYINDEX=yes ./nginx.SlackBuild

Extra modules not included with stock nginx can be added by passing
them as a variable to the slackbuild as a space separated list of
full paths to modules.  List of extra 3rd party modules available at
http://wiki.nginx.org/3rdPartyModules.

  ADDMODULE="/tmp/passenger-release-5.0.13/ext/nginx" ./nginx.SlackBuild

The optional dynamic loadable module support can be activated by setting
NGINX_DYNAMIC. The built modules are disabled by default, uncomment the
needed modules in /etc/nginx/modules.conf. Understand that the module
order in the configuration file is significant, so you may need to
reorder some lines, otherwise you'll get an error about undefined
symbols.

  NGINX_DYNAMIC=yes ./nginx.SlackBuild

To use lua module add lua_package_path line to nginx.conf

  http {
    lua_package_path "/usr/share/lua/5.1/resty/?.lua;;";

    # the rest of your configuration lines
    ...
  }

NOTE: NGINX is using luajit from openresty branch which is differ from
Slackware current stock package. To build NGINX with luajit support:
- removepkg luajit
- build and installpkg this repo development/luajit2

Build with QUIC/HTTP3 support requires boringssl or libressl or quictls.
Default is disable HTTP3 support. HTTP3 build variables:

  HTTP3=yes QUIC=libressl/boringssl/quictls ./nginx.SlackBuild
