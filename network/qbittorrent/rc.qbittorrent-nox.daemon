#!/bin/bash

# The daemon's name (to ensure uniqueness and for stop, restart and status)
name="qbittorrent-nox"

UIPORT="8080"
PRGUSER=""
if [ -z "$2" ]; then
    echo "ERROR: ${name} should not be running as root"
    echo "usage: $0 <start|stop|restart|reload|status> PRGUSER [UIPORT]"
    exit 1
else
    PRGUSER="$2"
    if [ -n "$3" ]; then
	UIPORT="$3"
    fi
fi

homedir=$( getent passwd "$PRGUSER" | cut -d: -f6 )

# The path of the client executable
command="/usr/bin/${name}"
# Any command line arguments for the client executable
command_args="--webui-port=$UIPORT"
# The path of the daemon executable
daemon="/usr/bin/daemon"

[ -x "$daemon" ] || exit 0
[ -x "$command" ] || exit 0

# Note: The following daemon option arguments could be in /etc/daemon.conf
# instead. That would probably be better because if the command itself were
# there as well then we could just use the name here to start the daemon.
# Here's some code to do it here in case you prefer that.

# Any command line arguments for the daemon executable (when starting)
daemon_start_args="--env=HOME=$homedir" # e.g. --inherit --env="ENV=VAR" --unsafe
# The pidfile directory (need to force this so status works for normal users)
pidfiles="/run/${name}-${UIPORT}"
# The user[:group] to run as (if not to be run as root)
user="${PRGUSER}"
# The path to chroot to (otherwise /)
chroot=""
# The path to chdir to (otherwise /)
chdir="$homedir"
# The umask to adopt, if any
umask=""
# The syslog facility or filename for the client's stdout (otherwise discarded)
stdout="/tmp/qbittorrent-nox-${UIPORT}.log"
# The syslog facility or filename for the client's stderr (otherwise discarded)
stderr="/tmp/qbittorrent-nox-${UIPORT}.log"

if [ ! -d $pidfiles ]; then
  mkdir -p $pidfiles
fi
chown $user $pidfiles

case "$1" in
    start)
	# This if statement isn't strictly necessary but it's user friendly
	if "$daemon" --running --name "$name" --pidfiles "$pidfiles"
	then
	    echo "$name is already running."
	else
	    echo -n "Starting $name..."
	    "$daemon" --respawn $daemon_start_args \
		--name "$name" --pidfiles "$pidfiles" \
		${user:+--user $user} ${chroot:+--chroot $chroot} \
		${chdir:+--chdir $chdir} ${umask:+--umask $umask} \
		${stdout:+--stdout $stdout} ${stderr:+--stderr $stderr} \
		-- \
		"$command" $command_args
	    echo done.
	fi
	;;

    stop)
	# This if statement isn't strictly necessary but it's user friendly
	if "$daemon" --running --name "$name" --pidfiles "$pidfiles"
	then
	    echo -n "Stopping $name..."
	    "$daemon" --stop --name "$name" --pidfiles "$pidfiles"
	    echo done.
	else
	    echo "$name is not running."
	fi
	;;

    restart|reload)
	if "$daemon" --running --name "$name" --pidfiles "$pidfiles"
	then
	    echo -n "Restarting $name..."
	    "$daemon" --restart --name "$name" --pidfiles "$pidfiles"
	    echo done.
	else
	    echo "$name is not running."
	    exit 1
	fi
	;;

    status)
	"$daemon" --running --name "$name" --pidfiles "$pidfiles" --verbose
	;;

    *)
	echo "usage: $0 <start|stop|restart|reload|status> PRGUSER [UIPORT]" >&2
	exit 1
esac

exit 0
