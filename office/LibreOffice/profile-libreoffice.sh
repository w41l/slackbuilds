export UNO_PATH=@SD_PROG@
export URE_BOOTSTRAP=vnd.sun.star.pathname:@SD_PROG@/fundamentalrc
export PATH=${PATH}:@SD_PROG@

# Select one of VCL Plugin to use
# export SAL_USE_VCLPLUGIN=gtk
# export SAL_USE_VCLPLUGIN=gtk3
# export SAL_USE_VCLPLUGIN=gtk3_kde5
# export SAL_USE_VCLPLUGIN=kf5
# export SAL_USE_VCLPLUGIN=qt5
