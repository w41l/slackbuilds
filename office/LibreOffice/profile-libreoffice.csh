setenv UNO_PATH @SD_PROG@
setenv URE_BOOTSTRAP vnd.sun.star.pathname:@SD_PROG@/fundamentalrc
setenv PATH \${PATH}:@SD_PROG@

# Select one of VCL Plugin to use
# setenv SAL_USE_VCLPLUGIN gtk3
# setenv SAL_USE_VCLPLUGIN gtk
# setenv SAL_USE_VCLPLUGIN kf5
# setenv SAL_USE_VCLPLUGIN qt5
# setenv SAL_USE_VCLPLUGIN gtk3_kde5
