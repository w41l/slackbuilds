config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

preserve_perms() {
  NEW="$1"
  OLD="$(dirname ${NEW})/$(basename ${NEW} .new)"
  if [ -e ${OLD} ]; then
    cp -a ${OLD} ${NEW}.incoming
    cat ${NEW} > ${NEW}.incoming
    mv ${NEW}.incoming ${NEW}
  fi
  config ${NEW}
}

config etc/audit/audisp-remote.conf.new
config etc/audit/audit-stop.rules.new
config etc/audit/auditd.conf.new
config etc/audit/plugins.d/af_unix.conf.new
config etc/audit/plugins.d/au-remote.conf.new
config etc/audit/plugins.d/audispd-zos-remote.conf.new
config etc/audit/plugins.d/syslog.conf.new
config etc/audit/zos-remote.conf.new
config etc/default/auditd.new
config etc/libaudit.conf.new
preserve_perms etc/rc.d/rc.auditd.new
